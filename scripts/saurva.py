from datetime import datetime
import os
import json
import sys
import time
sys.path.insert(0, 'packages')
import discord
from discord.errors import *
from discord.ext import commands

class Setup():
    """
    Class holding config we read from disk.
    """
    def __init__(self, config):
        self.bot_token = 'Undefined' if 'bot_token' not in config else config['bot_token']
        self.bot_prefix = 'Undefined' if 'bot_prefix' not in config else config['bot_prefix']
        self.server_id = 'Undefined' if 'server_id' not in config else config['server_id']
        self.server_name = 'Undefined' if 'server_name' not in config else config['server_name']
        self.admin_ids = [] if 'admin_ids' not in config else config['admin_ids']
        self.self_assignable_roles_channels = 'Undefined' if 'self_assignable_roles_channels' not in config else config['self_assignable_roles_channels'] 
        self.self_assignable_roles = {} if 'self_assignable_roles' not in config else config['self_assignable_roles']

# Constants
_SETUP = Setup(json.load(open(os.path.join(os.getcwd(), 'config/config.json'))))
_SESSION_LOG_FILE_NAME = f'log/{int(time.time())}.txt'
_LOCKDOWN = False

# Setup
intents = discord.Intents.default()
intents.messages = True

bot = commands.Bot(command_prefix=_SETUP.bot_prefix, intents=intents)

def write_to_log(content, print_to_console=False):
    """
    Writes the given content, prefixed with a localised timestamp, to the configured log file.
    Log files are stored under log/ in the current working directory.
    Each log corresponds to a single session.

    IN:
        - content - The string message to write to the log
        - print_to_console - True if the content should also be printed to the command prompt
    """
    content_with_stamp = f'{datetime.now().strftime("%c")}: {content}\n'
    with open(os.path.join(os.getcwd(), _SESSION_LOG_FILE_NAME), 'a+') as log_file:
        log_file.write(content_with_stamp)

    if (print_to_console):
        print(content_with_stamp)

def validate_command_ctx(ctx):
    """
    Returns True if the ctx given is appropriate for the bot to do something.
    """
    if not ctx.guild or ctx.guild.id != _SETUP.server_id or ctx.author == bot.user:
        return False 

    return True

def validate_admin_user(ctx):
    """
    Returns True if the user giving the command in the ctx is considered an administrator for the server.
    """
    if ctx.message.author.id not in _SETUP.admin_ids:
        return False

    return True

"""
Assigns the command author the specified role.
Note that the bot must have manage_role permissions.
Roles are defined in config.json, with an identifying key and corresponding ID.

Command format:
    ^s role {role name}

Example:
    ^s role member
"""
@bot.command()
async def role(ctx, role_name: str = ''):
    if not validate_command_ctx(ctx):
        await ctx.send('Sorry, you do not have permission to execute this command.')
        write_to_log(f'Role command failed; user does not have permission to execute this command.')
        return

    if not ctx.channel.id in _SETUP.self_assignable_roles_channels:
        return

    if _LOCKDOWN:
        await ctx.send(f"Sorry, but new role assignments are currently blocked. Please try again later.")
        await ctx.message.delete()
        return

    if len(role_name.replace(' ', '')) == 0:
        await ctx.send(f"Sorry, but you need to specify a role name!")
        return

    if _SETUP.self_assignable_roles.__contains__(role_name):
        try:
            await ctx.author.add_roles(
                (ctx.guild.get_role(_SETUP.self_assignable_roles.get(role_name))),
                reason=f"This role was self-assigned via bot.")

            await ctx.send(f"Done! {ctx.author.name} now has the **{role_name}** role!")
            write_to_log(f'Role {role_name} was successfully assigned to {ctx.author.name}.')
            await ctx.message.delete()

        except commands.MissingRequiredArgument:
            await ctx.send(f'Sorry, but you need to specify a role name!')

        except discord.Forbidden:
            await ctx.send(f'Sorry, you don\'t have permission to assign yourself this role.')
            write_to_log(f'Could not assign {role_name} to user {ctx.author.name}, as permissions and/or role hierarchy are insufficient.')

        except HTTPException:
            await ctx.send(f'Sorry, adding the role failed. Please try again later.')
            write_to_log(f'Could not assign {role_name} to user {ctx.author.name}; adding the role failed.')

    else:
        await ctx.send(f'Sorry, the role **{role_name}** does not exist.')
        write_to_log(f'Could not assign {role_name} to user {ctx.author.name}; role does not exist.')

    return

"""
Warns a user by delivering a DM with the given message.

Command format:
    ^s warn {user id} "{message}"

Example:
    ^s warn 123 "This is a warning."
"""
@bot.command()
async def warn(ctx, user_id: int = None, warn_reason: str = 'One or more rule violations.'):
    print(f'user id is {user_id}, warning is {warn_reason}')
    try:
        if not validate_command_ctx(ctx) or not validate_admin_user(ctx):
            await ctx.send('Sorry, you do not have permission to execute this command.')
            write_to_log(f'Warn command failed; user does not have permission to execute this command.')
        
        elif not user_id:
            raise commands.MissingRequiredArgument()

        else:
            if len(warn_reason.replace(' ', '')) == 0:
                warn_reason = 'No reason given.'

            user_to_warn = await bot.fetch_user(user_id)
            await user_to_warn.send(f'You have been warned by the moderation team of {_SETUP.server_name}: {warn_reason}')
            await ctx.send(f'Warning issued successfully!')
            write_to_log(f'Warning issued to user {user_to_warn.name} by {ctx.author.name}.')

    except (commands.MissingRequiredArgument, ValueError, TypeError):
        await ctx.send(f'Sorry, but one or more arguments were missing for this command. Usage: {_SETUP.bot_prefix} warn [user_id] "[message]"')
        write_to_log(f'Could not issue a warning, as one/more arguments were missing or invalid.')

    except NotFound:
        await ctx.send(f'Could not issue a warning, as a user matching Id {user_id} was not found.')
        write_to_log(f'Could not issue a warning, as a user matching Id {user_id} was not found.')

    except discord.Forbidden:
        await ctx.send(f'Could not issue a warning to user Id {user_id}, as they do not accept direct messages from bots.')
        write_to_log(f'Could not issue a warning to user Id {user_id}, as they do not accept direct messages from bots.')

    except HTTPException:
        await ctx.send(f'Could not issue a warning to user Id {user_id}, as the connection to Discord failed.')
        write_to_log(f'Could not issue a warning to user Id {user_id}, as the connection to Discord failed.')

    return

"""
Kicks the specified user from the server.
Note that admin users cannot be kicked.

Command format:
    ^s kick {user id}

Example:
    ^s kick 123
"""
@bot.command()
async def kick(ctx, user_id: int = None):
    try:
        if not validate_command_ctx(ctx) or not validate_admin_user(ctx):
            await ctx.send('Sorry, you do not have permission to execute this command.')
            write_to_log(f'Warn command failed; user does not have permission to execute this command.')
        
        elif not user_id:
            raise commands.MissingRequiredArgument()

        elif user_id in _SETUP.admin_ids:
            await ctx.send('Sorry, I can\'t kick members with admin roles.')
            write_to_log(f'Could not kick user Id {user_id}; cannot kick users with an admin role.')
        
        else:
            user_to_kick = await bot.fetch_user(user_id)
            await ctx.guild.kick(
                user=user_to_kick,
                reason="Kick given by bot."
            )
            await ctx.send(f'Kick issued successfully! **{user_to_kick.name}** has been booted.')
            write_to_log(f'Kick issued to user {user_to_kick.name} by {ctx.author.name}.')

    except (commands.MissingRequiredArgument, ValueError, TypeError):
        await ctx.send(f'Sorry, but one or more arguments were missing or invalid for this command. Usage: {_SETUP.bot_prefix} kick [user_id]')
        write_to_log(f'Could not issue a kick, as one/more arguments were missing or invalid.')
        
    except NotFound:
        await ctx.send(f'Could not issue a kick, as a user matching Id {user_id} was not found.')
        write_to_log(f'Could not issue a kick, as a user matching Id {user_id} was not found.')

    except discord.Forbidden:
        await ctx.send(f'Could not issue a kick to user Id {user_id}, as the bot has insufficient permissions.')
        write_to_log(f'Could not issue a kick to user Id {user_id},  as the bot has insufficient permissions.')

    except HTTPException:
        await ctx.send(f'Could not issue a kick to user Id {user_id}, as the connection to Discord failed.')
        write_to_log(f'Could not issue a kick to user Id {user_id}, as the connection to Discord failed.')

    return

"""
Bans the specified user from the server.
Note that admin users cannot be kicked.

Command format:
    ^s ban {user id} "{ban reason}"

Example:
    ^s ban 123 "Banned for spamming."
"""
@bot.command()
async def ban(ctx, user_id: int = None, ban_reason: str = "One or more rule violations."):
    try:
        if not validate_command_ctx(ctx) or not validate_admin_user(ctx):
            await ctx.send('Sorry, you do not have permission to execute this command.')
            write_to_log(f'Warn command failed; user does not have permission to execute this command.')
        
        elif not user_id:
            raise commands.MissingRequiredArgument()

        elif user_id in _SETUP.admin_ids:
            await ctx.send('Sorry, I can\'t ban members with admin roles.')
            write_to_log(f'Could not ban user Id {user_id}; cannot ban users with an admin role.')

        else:
            if len(ban_reason) == 0 or ban_reason == '':
                ban_reason = 'No reason given.'

            user_to_ban = await bot.fetch_user(user_id)
            await ctx.guild.ban(
                user=user_to_ban,
                reason=ban_reason,
                delete_message_days=1
            )
            await ctx.send(f'Ban issued successfully! **{user_to_ban.name}** has been permanently removed.')
            write_to_log(f'Ban issued to user {user_to_ban.name} by {ctx.author.name}.')

    except (commands.MissingRequiredArgument, ValueError, TypeError):
        await ctx.send(f'Sorry, but one or more arguments were missing or invalid for this command. Usage: {_SETUP.bot_prefix} ban [user_id] "[ban_reason]"')
        write_to_log(f'Could not issue a ban, as one/more arguments were missing or invalid.')

    except NotFound:
        await ctx.send(f'Could not issue a ban, as a user matching Id {user_id} was not found.')
        write_to_log(f'Could not issue a ban, as a user matching Id {user_id} was not found.')

    except discord.Forbidden:
        await ctx.send(f'Could not issue a ban to user Id {user_id}, as the bot has insufficient permissions.')
        write_to_log(f'Could not issue a ban to user Id {user_id},  as the bot has insufficient permissions.')

    except HTTPException:
        await ctx.send(f'Could not issue a ban to user Id {user_id}, as the connection to Discord failed.')
        write_to_log(f'Could not issue a ban to user Id {user_id}, as the connection to Discord failed.')

    return

"""
Toggles lockdown mode.
When in lockdown mode, the bot will not process any role changes.

Command format:
    ^s lockdown

Example:
    ^s lockdown
"""
@bot.command()
async def lockdown(ctx):
    if not validate_command_ctx(ctx) or not validate_admin_user(ctx):
        await ctx.send('Sorry, you do not have permission to execute this command.')
        write_to_log(f'Lockdown command failed; user does not have permission to execute this command.')

    else:
        global _LOCKDOWN
        _LOCKDOWN = not _LOCKDOWN

        if _LOCKDOWN:
            await ctx.send(f'Done! Role assignment lockdown enabled.')
        
        else:
            await ctx.send(f'Done! Role assignment lockdown disabled.')

    return

"""
Removes all messages by the given user from the channel where this is executed.

Command format:
    ^s purge [user_id] [1-999]

Example:
    ^s lockdown 123 100
"""
@bot.command()
async def purge(ctx, user_id: int = None, delete_count: int = None):
    try:
        if not validate_command_ctx(ctx) or not validate_admin_user(ctx):
            await ctx.send('Sorry, you do not have permission to execute this command.')
            write_to_log(f'Purge command failed; user does not have permission to execute this command.')

        elif not user_id or not delete_count or delete_count > 999:
            raise commands.MissingRequiredArgument()

        elif delete_count > 999:
            raise ValueError()

        else:
            await ctx.message.delete()

            deleted_message_count = 0
            async for message in ctx.channel.history(limit=delete_count + 1):
                if message.author.id == user_id:
                    try:
                        await message.delete()
                        deleted_message_count += 1

                    except:
                        pass

            purge_target_user = await bot.fetch_user(user_id)
            write_to_log(f'Purge ({delete_count} specified, {deleted_message_count} removed) messages for user {purge_target_user.name} by {ctx.author.name}.')
            await ctx.send(f'Purge complete! {deleted_message_count} messages sent by {purge_target_user} were deleted.')

    except (commands.MissingRequiredArgument, ValueError, TypeError):
        await ctx.send(f'Sorry, but one or more arguments were missing or invalid for this command. Usage: {_SETUP.bot_prefix} purge [user_id] [1-999]')
        write_to_log(f'Could not issue a purge, as one/more arguments were missing or invalid.')

    except NotFound:
        await ctx.send(f'Could not issue a purge, as a user matching Id {user_id} was not found.')
        write_to_log(f'Could not issue a purge, as a user matching Id {user_id} was not found.')

    except discord.Forbidden:
        await ctx.send(f'Could not issue a purge for user Id {user_id}, as the bot has insufficient permissions.')
        write_to_log(f'Could not issue a purge for user Id {user_id},  as the bot has insufficient permissions.')

    except HTTPException:
        await ctx.send(f'Could not issue a purge for user Id {user_id}, as the connection to Discord failed.')
        write_to_log(f'Could not issue a purge for user Id {user_id}, as the connection to Discord failed.')

    return

@bot.event
async def on_ready():
    """
    Called when the bot has successfully connected to Discord.
    Outputs some basic connection info, and performs some other setup steps.
    """
    write_to_log(f"Ready! Issue commands with prefix: {_SETUP.bot_prefix}", True)

# Entry point - attempt to connect to Discord
bot.run(_SETUP.bot_token)
bot.listen()
