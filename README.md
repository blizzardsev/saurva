# About saurva

Saurva is a simple Python-based Discord bot for performing basic server moderation.

## CI/deployment script configuration

### Dependencies

* **pigar**: Automatic Python requirements.txt @ https://github.com/damnever/pigar

## Python configuration

### Runtime

* Python 3.10

### Dependencies

* discord.py: Python Discord API @: https://discordpy.readthedocs.io/en/stable/

## Deployment

Use script *deploy.bat*; execute within Saurva directory. This is required *at least once* to allow Saurva to install required dependencies, etc.

After this, Saurva may be run using the command prompt as a Python file. Remember to include the parameters listed above.

As Saurva is a Discord bot, it should be hosted somewhere uptime can be guaranteed (I.E preferably not on a desktop PC).

## Configuration

Use `config.json` under config/ to determine runtime behaviour:
- **bot_token**: String Discord bot token to allow connectivity.
- **bot_prefix**: String prefix for all commands, E.G "^s"
- **server_id**: Integer ID of the server to moderate.
- **server_name**: String name of the server for use in messages.
- **admin_ids**: List of Integer IDs of members who are permitted to use Admin commands.
- **self_assignable_roles_channels**: List of channels where members may use the role command.
- **self_assignable_roles**: Mapping of String : Integer items describing roles and role IDs that can be assigned.

### Example configuration
```
{
    "bot_token": "my-bot-token",
    "bot_prefix": "^s ",
    "server_id": 123,
    "server_name": "My Awesome Server",
    "admin_ids": [
        444,
        555,
    ],
    "self_assignable_roles_channels": [
        666,
        777
    ],
    "self_assignable_roles": {
        "member": 888,
        "notify": 999
    }
}
```

## Commands

- **role**: Adds the specified role to the command author.
    - *Usage*: {prefix} role {role name}
    - *Example*: `^s role member`
- **warn**: Warns the specified user with a given message.
    - *Usage*: {prefix} warn {user id} "{message}"
    - *Example*: `^s warn 123 "This is a warning."`
- **kick**: Kicks the specified user.
    - *Usage*: {prefix} kick {role name}
    - *Example*: `^s kick 123`
- **ban**: Bans the specified user from the server, recording the reason in the audit log.
    - *Usage*: {prefix} ban {user id} "{ban reason}"
    - *Example*: `^s ban 123 "Banned for spamming."`

## Contact

Please raise any suggestions, etc. using the Issue Tracker.

Email: *blizzardsev.dev@gmail.com*
