@echo off
Rem This script will automatically deduce and install the dependencies for saurva, before starting it.

echo Fetching requirements...
rem Builds requirements.txt, whilst ignoring any packages already present
cd scripts
pigar -i packages
cd ..

echo Downloading dependencies...
rem Installs (non-standard with Python) dependencies to the packages directory
pip install -r scripts/requirements.txt -t scripts/packages --upgrade

echo Deploying..
cd scripts
python saurva.py
echo Deployment complete.

echo Script completed. Last updated: %time%
pause
